package ru.ahmetahunov.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.IAbstractService;
import ru.ahmetahunov.tm.api.service.EntityManagerService;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import javax.persistence.EntityManager;
import java.util.List;

@RequiredArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements IAbstractService<T> {

    @NotNull
    private final EntityManagerService entityManagerService;

    @NotNull
    protected EntityManager getEntityManager() {
        return entityManagerService.getEntityManagerFactory().createEntityManager();
    }

    @Override
    public abstract void persist(@Nullable final T item) throws InterruptedOperationException;

    @Override
    public abstract void merge(@Nullable final T item) throws InterruptedOperationException;

    @Nullable
    @Override
    public abstract T findOne(@Nullable final String id);

    @NotNull
    @Override
    public abstract List<T> findAll();

    @Override
    public abstract void remove(@Nullable final String id) throws InterruptedOperationException;

}
