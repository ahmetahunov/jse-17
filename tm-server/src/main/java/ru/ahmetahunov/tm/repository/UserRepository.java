package ru.ahmetahunov.tm.repository;

import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@RequiredArgsConstructor
public class UserRepository implements IUserRepository {

	@NotNull
	private final EntityManager entityManager;

	@Override
	public void persist(@NotNull final User user) {
		entityManager.persist(user);
	}

	@Override
	public void merge(@NotNull final User user) {
		entityManager.merge(user);
	}

	@Nullable
	@Override
	public User findOne(@NotNull final String id) {
		@NotNull final TypedQuery<User> query = entityManager.createQuery(
				"SELECT u FROM User u WHERE u.id = :id",
				User.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("id", id);
		@NotNull final List<User> users = query.getResultList();
		return (users.isEmpty()) ? null : users.get(0);
	}

	@Nullable
	@Override
	public User findUser(@NotNull final String login) {
		@NotNull final TypedQuery<User> query = entityManager.createQuery(
				"SELECT u FROM User u WHERE u.login = :login",
				User.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("login", login);
		@NotNull final List<User> users = query.getResultList();
		return (users.isEmpty()) ? null : users.get(0);
	}

	@NotNull
	@Override
	public List<User> findAll() {
		@NotNull final TypedQuery<User> query = entityManager.createQuery(
				"SELECT u FROM User u",
				User.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		return query.getResultList();
	}

	@Override
	public void remove(@NotNull final String id) throws InterruptedOperationException {
		@Nullable final User user = findOne(id);
		if (user == null) throw new InterruptedOperationException();
		entityManager.remove(user);
	}

	@Override
	public void removeAll() {
		for (@NotNull final User user : findAll())
			entityManager.remove(user);
	}

}
