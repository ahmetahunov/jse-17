package ru.ahmetahunov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ISessionService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@Getter
@Setter
@XmlType
@NoArgsConstructor
public final class UserDTO extends AbstractEntityDTO implements Serializable {

    @NotNull
    private String login = "";

    @NotNull
    private String password = "";

    @NotNull
    private Role role = Role.USER;

    @NotNull
    public User transformToUser(@NotNull final ServiceLocator serviceLocator) {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        @NotNull final User user = new User();
        user.setId(this.id);
        user.setLogin(this.login);
        user.setRole(this.role);
        user.setProjects(projectService.findAll(this.id));
        user.setTasks(taskService.findAll(this.id));
        user.setSession(sessionService.findAll(this.id));
        return user;
    }

}
