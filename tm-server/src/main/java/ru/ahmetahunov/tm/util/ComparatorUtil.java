package ru.ahmetahunov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ComparatorUtil {

	@NotNull
	public static String getComparator(@Nullable final String comparator) {
		if (comparator == null || comparator.isEmpty()) return "name";
		switch (comparator.toLowerCase()) {
			case ("creationdate"): return "creationDate";
			case ("startdate"): return "startDate";
			case ("finishdate"): return "finishDate";
			case ("status"): return "status";
			default: return "name";
		}
	}

}
