package ru.ahmetahunov.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class InterruptedOperationException extends Exception {

    public InterruptedOperationException() {}

    public InterruptedOperationException(@Nullable final Exception e) {
        super("Operation failed!", e);
    }

    public InterruptedOperationException(@Nullable final String e) {
        super(e);
    }

}
