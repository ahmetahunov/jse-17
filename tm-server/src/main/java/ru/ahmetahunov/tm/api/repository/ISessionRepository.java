package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import java.util.List;

public interface ISessionRepository {

	public void persist(@NotNull Session session);

	public void merge(@NotNull Session session);

	@Nullable
	public Session findOne(@NotNull String id);

	@Nullable
	public Session findOneByUserId(@NotNull String id, @NotNull String userId);

	@NotNull
	public List<Session> findAll();

	@NotNull
	public List<Session> findAll(@NotNull String userId);

	public void remove(@NotNull String id) throws InterruptedOperationException;

	public void removeByUserId(@NotNull String id, @NotNull String userId) throws InterruptedOperationException;

}
