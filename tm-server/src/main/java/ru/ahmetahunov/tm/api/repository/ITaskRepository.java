package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import java.util.List;

public interface ITaskRepository {

    public void persist(@NotNull Task Task);

    public void merge(@NotNull Task Task);

    @Nullable
    public Task findOne(@NotNull String id);

    @Nullable
    public Task findOneById(@NotNull String userId, @NotNull String taskId);

    @NotNull
    public List<Task> findAll();
    
    @NotNull
    public List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    public List<Task> findAllWithComparator(@NotNull String userId, @NotNull String comparator);

    @NotNull
    public List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId, @NotNull String comparator);

    @NotNull
    public List<Task> findByName(@NotNull String userId, @NotNull String taskName);

    @NotNull
    public List<Task> findByNameOrDesc(@NotNull String userId, @NotNull String searchPhrase);

    @NotNull
    public List<Task> findByDescription(@NotNull String userId, @NotNull String description);

    public void remove(@NotNull String id) throws InterruptedOperationException;

    public void removeById(@NotNull String userId, @NotNull String taskId) throws InterruptedOperationException;

    public void removeAll(@NotNull String userId);

}
