package ru.ahmetahunov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.AdminEndpoint;
import ru.ahmetahunov.tm.api.service.IPropertyService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.dto.SessionDTO;
import ru.ahmetahunov.tm.dto.UserDTO;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.util.CipherUtil;
import ru.ahmetahunov.tm.util.PassUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.ahmetahunov.tm.api.endpoint.AdminEndpoint")
public final class AdminEndpointImpl implements AdminEndpoint {

	private ServiceLocator serviceLocator;

	public AdminEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	@NotNull
	@Override
	@WebMethod
	public List<UserDTO> findAllUsers(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@NotNull final SessionDTO session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		@NotNull final List<UserDTO> users = new ArrayList<>();
		for (@NotNull final User user : serviceLocator.getUserService().findAll())
			users.add(user.transformToDTO());
		return users;
	}

	@Override
	@WebMethod
	public void userUpdatePasswordAdmin(
			@WebParam(name = "session") final String token,
			@WebParam(name = "userId") final String userId,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@NotNull final SessionDTO session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getUserService().updatePasswordAdmin(userId, password);
	}

	@Override
	@WebMethod
	public void userChangeRoleAdmin(
			@WebParam(name = "session") final String token,
			@WebParam(name = "userId") final String userId,
			@WebParam(name = "role") final Role role
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@NotNull final SessionDTO session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getUserService().updateRole(userId, role);
	}

	@Override
	@WebMethod
	public void userRemove(
			@WebParam(name = "session") final String token,
			@WebParam(name = "userId") final String userId
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@NotNull final SessionDTO session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getUserService().remove(userId);
	}

	@Override
	@WebMethod
	public UserDTO userRegisterAdmin(
			@WebParam(name = "session") final String token,
			@WebParam(name = "user") final String login,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@NotNull final SessionDTO session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		@NotNull final User user = new User();
		user.setLogin(login);
		user.setPassword(PassUtil.getHash(password));
		serviceLocator.getUserService().persist(user);
		@Nullable final User found = serviceLocator.getUserService().findOne(user.getId());
		return (found == null) ? null : found.transformToDTO();
	}

	@Nullable
	@Override
	@WebMethod
	public UserDTO userFindByLogin(
			@WebParam(name = "session") final String token,
			@WebParam(name = "login") final String login
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@NotNull final SessionDTO session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		@Nullable final User user = serviceLocator.getUserService().findUser(login);
		return (user == null) ? null : user.transformToDTO();
	}

}
