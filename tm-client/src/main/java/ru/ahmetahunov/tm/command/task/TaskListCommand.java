package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import java.lang.Exception;

@NoArgsConstructor
public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all available tasks.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = serviceLocator.getStateService().getSession();
        @NotNull final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[TASK LIST]");
        @NotNull final String comparator =
                terminalService.getAnswer("Enter sort type<creationDate|startDate|finishDate|status>: ");
        int i = 1;
        for (@NotNull final TaskDTO task : taskEndpoint.findAllTasks(session, comparator)) {
            terminalService.writeMessage(String.format("%d. %s ID:%s", i++, task.getName(), task.getId()));
        }
    }

}