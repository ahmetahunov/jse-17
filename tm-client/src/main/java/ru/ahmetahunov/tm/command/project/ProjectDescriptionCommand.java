package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.ProjectDTO;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.exception.FailedOperationException;
import ru.ahmetahunov.tm.util.InfoUtil;

@NoArgsConstructor
public final class ProjectDescriptionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-info";
    }

    @NotNull
    @Override
    public String getDescription() { return "Show project's information."; }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String session = serviceLocator.getStateService().getSession();
        terminalService.writeMessage("[PROJECT-DESCRIPTION]");
        @NotNull final String projectId = terminalService.getAnswer("Please enter project id: ");
        @Nullable final ProjectDTO project = serviceLocator.getProjectEndpoint().findOneProject(session, projectId);
        if (project == null) throw new FailedOperationException("Selected project does not exist.");
        terminalService.writeMessage(InfoUtil.getProjectInfo(project));
    }

}
