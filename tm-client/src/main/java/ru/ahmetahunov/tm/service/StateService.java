package ru.ahmetahunov.tm.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.IStateService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import java.util.Collection;
import java.util.Map;

@RequiredArgsConstructor
public final class StateService implements IStateService {

    @NotNull
    private final Map<String, AbstractCommand> commands;

    @Getter
    @Setter
    @Nullable
    private String session;

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Nullable
    @Override
    public AbstractCommand getCommand(@Nullable final String operation) {
        if (operation == null || operation.isEmpty()) return null;
        @Nullable final AbstractCommand command = commands.get(operation);
        if (command == null) return commands.get("unknown");
        return command;
    }

}
