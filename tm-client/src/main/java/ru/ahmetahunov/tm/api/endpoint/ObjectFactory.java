
package ru.ahmetahunov.tm.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.ahmetahunov.tm.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AccessForbiddenException_QNAME = new QName("http://endpoint.api.tm.ahmetahunov.ru/", "AccessForbiddenException");
    private final static QName _InterruptedOperationException_QNAME = new QName("http://endpoint.api.tm.ahmetahunov.ru/", "InterruptedOperationException");
    private final static QName _CreateUser_QNAME = new QName("http://endpoint.api.tm.ahmetahunov.ru/", "createUser");
    private final static QName _CreateUserResponse_QNAME = new QName("http://endpoint.api.tm.ahmetahunov.ru/", "createUserResponse");
    private final static QName _FindUser_QNAME = new QName("http://endpoint.api.tm.ahmetahunov.ru/", "findUser");
    private final static QName _FindUserResponse_QNAME = new QName("http://endpoint.api.tm.ahmetahunov.ru/", "findUserResponse");
    private final static QName _UpdateLogin_QNAME = new QName("http://endpoint.api.tm.ahmetahunov.ru/", "updateLogin");
    private final static QName _UpdateLoginResponse_QNAME = new QName("http://endpoint.api.tm.ahmetahunov.ru/", "updateLoginResponse");
    private final static QName _UpdatePassword_QNAME = new QName("http://endpoint.api.tm.ahmetahunov.ru/", "updatePassword");
    private final static QName _UpdatePasswordResponse_QNAME = new QName("http://endpoint.api.tm.ahmetahunov.ru/", "updatePasswordResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.ahmetahunov.tm.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AccessForbiddenException }
     * 
     */
    public AccessForbiddenException createAccessForbiddenException() {
        return new AccessForbiddenException();
    }

    /**
     * Create an instance of {@link InterruptedOperationException }
     * 
     */
    public InterruptedOperationException createInterruptedOperationException() {
        return new InterruptedOperationException();
    }

    /**
     * Create an instance of {@link CreateUser }
     * 
     */
    public CreateUser createCreateUser() {
        return new CreateUser();
    }

    /**
     * Create an instance of {@link CreateUserResponse }
     * 
     */
    public CreateUserResponse createCreateUserResponse() {
        return new CreateUserResponse();
    }

    /**
     * Create an instance of {@link FindUser }
     * 
     */
    public FindUser createFindUser() {
        return new FindUser();
    }

    /**
     * Create an instance of {@link FindUserResponse }
     * 
     */
    public FindUserResponse createFindUserResponse() {
        return new FindUserResponse();
    }

    /**
     * Create an instance of {@link UpdateLogin }
     * 
     */
    public UpdateLogin createUpdateLogin() {
        return new UpdateLogin();
    }

    /**
     * Create an instance of {@link UpdateLoginResponse }
     * 
     */
    public UpdateLoginResponse createUpdateLoginResponse() {
        return new UpdateLoginResponse();
    }

    /**
     * Create an instance of {@link UpdatePassword }
     * 
     */
    public UpdatePassword createUpdatePassword() {
        return new UpdatePassword();
    }

    /**
     * Create an instance of {@link UpdatePasswordResponse }
     * 
     */
    public UpdatePasswordResponse createUpdatePasswordResponse() {
        return new UpdatePasswordResponse();
    }

    /**
     * Create an instance of {@link UserDTO }
     * 
     */
    public UserDTO createUserDTO() {
        return new UserDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccessForbiddenException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AccessForbiddenException }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.ahmetahunov.ru/", name = "AccessForbiddenException")
    public JAXBElement<AccessForbiddenException> createAccessForbiddenException(AccessForbiddenException value) {
        return new JAXBElement<AccessForbiddenException>(_AccessForbiddenException_QNAME, AccessForbiddenException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InterruptedOperationException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link InterruptedOperationException }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.ahmetahunov.ru/", name = "InterruptedOperationException")
    public JAXBElement<InterruptedOperationException> createInterruptedOperationException(InterruptedOperationException value) {
        return new JAXBElement<InterruptedOperationException>(_InterruptedOperationException_QNAME, InterruptedOperationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.ahmetahunov.ru/", name = "createUser")
    public JAXBElement<CreateUser> createCreateUser(CreateUser value) {
        return new JAXBElement<CreateUser>(_CreateUser_QNAME, CreateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.ahmetahunov.ru/", name = "createUserResponse")
    public JAXBElement<CreateUserResponse> createCreateUserResponse(CreateUserResponse value) {
        return new JAXBElement<CreateUserResponse>(_CreateUserResponse_QNAME, CreateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.ahmetahunov.ru/", name = "findUser")
    public JAXBElement<FindUser> createFindUser(FindUser value) {
        return new JAXBElement<FindUser>(_FindUser_QNAME, FindUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.ahmetahunov.ru/", name = "findUserResponse")
    public JAXBElement<FindUserResponse> createFindUserResponse(FindUserResponse value) {
        return new JAXBElement<FindUserResponse>(_FindUserResponse_QNAME, FindUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateLogin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.ahmetahunov.ru/", name = "updateLogin")
    public JAXBElement<UpdateLogin> createUpdateLogin(UpdateLogin value) {
        return new JAXBElement<UpdateLogin>(_UpdateLogin_QNAME, UpdateLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateLoginResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.ahmetahunov.ru/", name = "updateLoginResponse")
    public JAXBElement<UpdateLoginResponse> createUpdateLoginResponse(UpdateLoginResponse value) {
        return new JAXBElement<UpdateLoginResponse>(_UpdateLoginResponse_QNAME, UpdateLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePassword }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdatePassword }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.ahmetahunov.ru/", name = "updatePassword")
    public JAXBElement<UpdatePassword> createUpdatePassword(UpdatePassword value) {
        return new JAXBElement<UpdatePassword>(_UpdatePassword_QNAME, UpdatePassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePasswordResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdatePasswordResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.ahmetahunov.ru/", name = "updatePasswordResponse")
    public JAXBElement<UpdatePasswordResponse> createUpdatePasswordResponse(UpdatePasswordResponse value) {
        return new JAXBElement<UpdatePasswordResponse>(_UpdatePasswordResponse_QNAME, UpdatePasswordResponse.class, null, value);
    }

}
